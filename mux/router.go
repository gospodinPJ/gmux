package mux

import (
	"net/http"
	"errors"
	"strings"
)

func filter(p []string, arg string) []string {
	res := p[:0]
	for _, x := range p {
		if x == arg {
			continue
		}
		res = append(res, x)
	}
	return res
}

type Context struct {
	path []string
	data interface{}
}

func new_context(path []string, data interface{}) *Context {
	return &Context{
		path: path,
		data: data,
	}
}

func NewContext(data interface{}) *Context {
	return new_context(nil, data)
}

func (ctx *Context) GetPath() []string {
	return ctx.path
}

func (ctx *Context) GetData() interface{} {
	return ctx.data
}

type handler_func func(http.ResponseWriter, *http.Request, *Context)
type err_handler_func func(http.ResponseWriter, *http.Request)

type Router struct {
	methods_map  map[string]*node
	err_handlers []err_handler_func
}

func NewRouter() *Router {
	m := make(map[string]*node)
	m["GET"] = nil
	m["POST"] = nil
	m["PUT"] = nil
	m["DELETE"] = nil

	return &Router{
		methods_map:  m,
		err_handlers: make([]err_handler_func, 600),
	}
}

func (r *Router) add_new_handler(method string, path string, f handler_func, ctx *Context) error {
	if f == nil {
		return errors.New("handler is nil")
	}

	var path_chunks []string
	path_chunks = filter(strings.Split(path, "/"), "")

	if r.methods_map[method] == nil {
		// empty node
		r.methods_map[method] = new_node("")
	}
	return r.methods_map[method].add_route(path_chunks, f, ctx)
}

func (r *Router) GET(path string, fn handler_func, ctx *Context) {
	r.add_new_handler("GET", path, fn, ctx)
}

func (r *Router) POST(path string, fn handler_func, ctx *Context) {
	r.add_new_handler("POST", path, fn, ctx)
}

func (r *Router) PUT(path string, fn handler_func, ctx *Context) {
	r.add_new_handler("PUT", path, fn, ctx)
}

func (r *Router) DELETE(path string, fn handler_func, ctx *Context) {
	r.add_new_handler("DELETE", path, fn, ctx)
}

func (r *Router) SetErrorHandler(err int, handler err_handler_func) {
	if err >= 0 && err < len(r.err_handlers) {
		r.err_handlers[err] = handler
	}
}

func (r *Router) RemoveErrorHandler(err int) {
	if err >= 0 && err < len(r.err_handlers) {
		r.err_handlers[err] = nil
	}
}

func (r *Router) ServeHTTP(wr http.ResponseWriter, req *http.Request) {
	if node, ok := r.methods_map[req.Method]; ok {
		var uri []string
		uri = filter(strings.Split(req.RequestURI, "/"), "")

		fn, ctx := node.search(uri)
		if fn != nil {
			fn(wr, req, ctx)
			return
		} else {
			if h := r.err_handlers[http.StatusNotFound]; h != nil {
				h(wr, req)
			} else {
				wr.Write([]uint8("404"))
			}
		}
	} else {
		if h := r.err_handlers[http.StatusMethodNotAllowed]; h != nil {
			h(wr, req)
		} else {
			wr.Write([]uint8("405"))
		}
	}
}
