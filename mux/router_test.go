package mux

import (
	"net/http"
	"testing"
)

func TestNewRouter(t *testing.T) {
	r := NewRouter()

	if r.methods_map == nil {
		t.Error("map with methods must not be nil")
	}

	if len(r.methods_map) != 4 {
		t.Error("map with methods must contain 4 method")
	}
}

func Test_add_new_handler(t *testing.T) {
	r := NewRouter()
	method := "GET"

	if err := r.add_new_handler(method, "test", func(http.ResponseWriter, *http.Request, *Context) {}, nil); err != nil {
		t.Error(err.Error())
	}
	if r.methods_map[method] == nil {
		t.Error("New node hasn't been created")
		return
	}
	if len(r.methods_map[method].children) != 1 {
		t.Error("Node 'test' hasn't been added in parent node")
	}

	r.add_new_handler(method, "/foo///bar/", func(http.ResponseWriter, *http.Request, *Context) {}, nil)
	if len(r.methods_map[method].children) != 2 {
		t.Error("Nodes '/foo///bar/' haven't been added in parent node")
	}

	r.add_new_handler(method, "//<param>/", func(http.ResponseWriter, *http.Request, *Context) {}, nil)
	if r.methods_map[method].param_node == nil {
		t.Error("Node '<param>' is parameter")
	}

	r.add_new_handler(method, "/foo//<param>/", func(http.ResponseWriter, *http.Request, *Context) {}, nil)
	if len(r.methods_map[method].children) != 2 {
		t.Error("Node '/foo' must contain parameter: [foo] -> <param>")
	}

	if err := r.add_new_handler(method, "test", nil, nil); err == nil {
		t.Error("node 'test' has already been added")
	}
}
