package mux

import (
	"net/http"
	"testing"
)

func Test_add_route(t *testing.T) {
	node := new_node("")

	if err := node.add_route([]string{"test"}, nil, nil); err != nil {
		t.Error(err.Error())
	}
	if len(node.children) != 1 {
		t.Error("node 'test' hasn't been added in parent node")
	}

	// next node
	if err := node.add_route([]string{"test", "foo"}, func(http.ResponseWriter, *http.Request, *Context) {}, nil); err != nil {
		t.Error(err.Error())
	}
	if len(node.children) != 1 {
		t.Error("node 'foo' must be added in 'test' node ")
	}
	if len(node.children["test"].children) != 1 {
		t.Error("node 'foo' hasn't been added in parent node")
	}

	// rewrite next node
	if err := node.add_route([]string{"test", "foo"}, nil, nil); err == nil {
		t.Error("node '/test/foo' has already been added")
	}
	if len(node.children["test"].children) != 1 {
		t.Error("node '/test/foo' has already been added")
	}

	// parameter node
	if err := node.add_route([]string{"test", "<param>"}, nil, nil); err != nil {
		t.Error(err.Error())
	}
	if len(node.children["test"].children) != 1 {
		t.Error("node '<param>' must be added as parameter")
	}
	if node.children["test"].param_node == nil {
		t.Error("node '<param>' must be added as parameter")
	}

	// rewrite parameter node
	if err := node.add_route([]string{"test", "<param>"}, nil, nil); err == nil {
		t.Error("node test/<param> has already been added")
	}

	// any path
	if err := node.add_route([]string{"test", "*"}, func(http.ResponseWriter, *http.Request, *Context) {}, nil); err != nil {
		t.Error(err.Error())
	}
	if node.children["test"].any_path == nil {
		t.Error("node 'test/*' hasn't been added in parent node")
	}

	// rewrite any path
	if err := node.add_route([]string{"test", "*"}, nil, nil); err == nil {
		t.Error("node '/test/*' has already been added")
	}
	if node.children["test"].any_path == nil {
		t.Error("node 'test/*' hasn't been added in parent node")
	}
}

func Test_search(t *testing.T) {
	str := ""
	h1 := func(http.ResponseWriter, *http.Request, *Context) { str = "h1" }
	h2 := func(http.ResponseWriter, *http.Request, *Context) { str = "h2" }
	h3 := func(http.ResponseWriter, *http.Request, *Context) { str = "h3" }
	h4 := func(http.ResponseWriter, *http.Request, *Context) { str = "h4" }
	h5 := func(http.ResponseWriter, *http.Request, *Context) { str = "h5" }
	h6 := func(http.ResponseWriter, *http.Request, *Context) { str = "h6" }

	node := new_node("")

	if err := node.add_route([]string{"test"}, h1, nil); err != nil {
		t.Error(err.Error())
	}
	if err := node.add_route([]string{"test", "foo"}, h2, nil); err != nil {
		t.Error(err.Error())
	}
	if err := node.add_route([]string{"test", "foo", "<param>"}, h3, nil); err != nil {
		t.Error(err.Error())
	}
	if err := node.add_route([]string{"test", "<param>", "foo"}, h4, nil); err != nil {
		t.Error(err.Error())
	}
	if err := node.add_route([]string{"test", "*"}, h5, nil); err != nil {
		t.Error(err.Error())
	}
	if err := node.add_route([]string{"test", "foo", "*"}, h6, nil); err != nil {
		t.Error(err.Error())
	}

	if f, _ := node.search([]string{"test"}); f == nil {
		t.Error("handler is nil")
	} else {
		f(nil, nil, nil)
	}
	if str != "h1" {
		t.Error("handler is invalid: " + str)
	}

	if f, _ := node.search([]string{"test", "foo"}); f == nil {
		t.Error("handler is nil")
	} else {
		f(nil, nil, nil)
	}
	if str != "h2" {
		t.Error("handler is invalid: " + str)
	}

	if f, _ := node.search([]string{"test", "foo", "bar"}); f == nil {
		t.Error("handler is nil")
	} else {
		f(nil, nil, nil)
	}
	if str != "h3" {
		t.Error("handler is invalid: " + str)
	}

	if f, _ := node.search([]string{"test", "bar", "foo"}); f == nil {
		t.Error("handler is nil")
	} else {
		f(nil, nil, nil)
	}
	if str != "h4" {
		t.Error("handler is invalid: " + str)
	}

	if f, _ := node.search([]string{"test", "aaa", "bbb", "ccc"}); f == nil {
		t.Error("handler is nil")
	} else {
		f(nil, nil, nil)
	}
	if str != "h5" {
		t.Error("handler is invalid: " + str)
	}

	if f, _ := node.search([]string{"test", "foo", "bbb", "ccc"}); f == nil {
		t.Error("handler is nil")
	} else {
		f(nil, nil, nil)
	}
	if str != "h6" {
		t.Error("handler is invalid: " + str)
	}
}

func TestContext(t *testing.T) {
	val := 42
	handler := func(wr http.ResponseWriter, r *http.Request, ctx *Context) {
		*(ctx.data.(*int)) = 43
	}

	node := new_node("")

	if err := node.add_route([]string{"test"}, handler, NewContext(&val)); err != nil {
		t.Error(err.Error())
	}

	if f, ctx := node.search([]string{"test"}); f == nil {
		t.Error("handler is nil")
	} else {
		f(nil, nil, ctx)
	}

	if val != 43 {
		t.Errorf("Context hasn't been processed; val = %d", val)
	}
}
