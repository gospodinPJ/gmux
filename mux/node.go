package mux

import (
	"errors"
)

type node struct {
	// name of this node
	name string

	// related nodes
	// /foo can save nodes /foo/test1, foo/test2, /foo/...
	children map[string]*node

	// node can be represented as parameter: /foo/<param>
	// the parameter will be replaced with url: /foo/some-addr/
	param_node *node

	// node can contain any path:
	// /foo/* can be /foo/test or /foo/test/bar/...
	// '*' will be replaced with any url
	any_path handler_func

	// handler function
	fn handler_func

	// context of this node:
	// string slice with full path of request ( /foo/* -> http://localhost/foo/bar/test -> {"bar", "test"} )
	// + any useful data
	ctx *Context
}

func new_node(n string) *node {
	return &node{
		name:       n,
		children:   make(map[string]*node),
		param_node: nil,
		any_path:   nil,
		fn:         nil,
	}
}

// Add new http route
// @param p is the slice with link's chunks:
// /books/year/1990 -> ['books', 'year', '1990']
// @param f is the handler for this route
// @param ctx is the context of this node (request)
//
// all links are represented as a tree with nodes.
// Every node can contain pointer to handler:
//
// '/books/year/1990' is split: node('books') -> node('year') -> node('1990')
// second link is added to the tree:
// '/book/year/1991': node('books') -> node('year') -> [node('1990'), node('1991')]
//
// @return error or nil
func (node *node) add_route(p []string, f handler_func, ctx *Context) error {
	current_node := node

	for i, leaf := range p {
		// if current node (for ex 'books') already contains the pointer to the
		// next chunk (for ex we had added '/books/' and now we add '/books/year/')
		if val, ok := current_node.children[leaf]; ok {
			// if leaf is a last chunk of the path
			if i == len(p)-1 {
				if val.fn != nil {
					return errors.New("This path already exists")
				} else {
					val.fn = f
					val.ctx = ctx
				}
				break
			}
			current_node = val
		} else {
			// if current chunk is any url (*):
			// /book/*
			if leaf == "*" {
				if current_node.any_path != nil {
					return errors.New("This path already exists")
				}
				current_node.any_path = f
				current_node.ctx = ctx
				return nil
			}

			// if tree doesn't contain this chunk
			new_node := new_node(leaf)

			// if this is the full path
			if i == len(p)-1 {
				new_node.fn = f
				new_node.ctx = ctx
			}

			// if current chunk is a param: /book/<time>/something
			if leaf[0] == '<' && leaf[len(leaf)-1] == '>' {
				if current_node.param_node != nil {
					return errors.New("This path already contains parametrized argument")
				}
				current_node.param_node = new_node
			} else {
				// add new node to the tree
				current_node.children[leaf] = new_node
			}

			current_node = new_node
		}
	}

	return nil
}

// Search path in the our tree
// @param p is a slice with link's chunks:
// /books/year/1990 -> ['books', 'year', '1990']
//
// @return handle for this path (function-callback with args) or nil and its context
func (n *node) search(p []string) (handler_func, *Context) {
	current_node := n
	nodes_tree := make([]*node, 0, len(p))
	params := make([]string, 0, len(p))

	for i, x := range p {
		// if node contains current link's chunk or node has parameter:
		// for ex we search link '/books/year/1992/test_book'
		// both links '/books/year/1992/test_book' && '/books/year/<value_year>/test_book'
		// will be correct

		if val, ok := current_node.children[x]; ok {
			nodes_tree = append(nodes_tree, val)
			if i == len(p)-1 {
				// if last part of URL is parameter
				if val.fn == nil && current_node.param_node != nil {
					return current_node.param_node.fn, current_node.param_node.ctx
				} else {
					if val.fn == nil {
						return nil, nil
					}
					return val.fn, val.ctx
				}
			} else {
				current_node = val
			}
		} else if current_node.param_node != nil {
			nodes_tree = append(nodes_tree, current_node.param_node)
			if i == len(p)-1 {
				params = append(params, x)
				if current_node.param_node.ctx != nil {
					current_node.param_node.ctx.path = params
					return current_node.param_node.fn, current_node.param_node.ctx
				}
				return current_node.param_node.fn, new_context(params, nil)
			} else {
				current_node = current_node.param_node
				params = append(params, x)
			}
		} else if current_node.any_path != nil {
			for _, y := range p[i:] {
				params = append(params, y)
			}
			if current_node.ctx != nil {
				current_node.ctx.path = params
				return current_node.any_path, current_node.ctx
			}
			return current_node.any_path, new_context(params, nil)
		} else {
			//reverse search '*' in the tree of nodes
			for i := len(nodes_tree) - 1; i >= 0; i-- {
				if nodes_tree[i].any_path != nil {
					params = p[i+1:]
					if nodes_tree[i].ctx != nil {
						nodes_tree[i].ctx.path = params
					}
					return nodes_tree[i].any_path, new_context(params, nil)
				}
			}
			break
		}
	}
	return nil, nil
}

