# GMux #
GMux is a tiny and fast http router. It supports simple routers, dynamic routes, http errors and user's context.


# Example #

```
#!go

func dummy_handler(w http.ResponseWriter, r *http.Request, ctx *mux.Context) {
	fmt.Println("'dummy' handler")
	w.Write([]uint8("'dummy' handler"))
}

func param_handler(w http.ResponseWriter, r *http.Request, ctx *mux.Context) {
	path := ctx.GetPath()
	fmt.Println("'param' handler; param is", path)
	w.Write([]uint8("'param' handler"))
}

func any_handler(w http.ResponseWriter, r *http.Request, ctx *mux.Context) {
	path := ctx.GetPath()
	fmt.Println("'any' handler; param is", path)
	w.Write([]uint8("'any' handler"))
}

func ctx_handler(w http.ResponseWriter, r *http.Request, ctx *mux.Context) {
	fmt.Println("'ctx' handler; ctx data is", ctx.GetData())
	w.Write([]uint8("'ctx' handler"))
}

func main() {
	r := mux.NewRouter()
	r.SetErrorHandler(http.StatusNotFound, func(w http.ResponseWriter, r *http.Request) {
		w.Write([]uint8("404"))
	})
	r.SetErrorHandler(http.StatusMethodNotAllowed, func(w http.ResponseWriter, r *http.Request) {
		w.Write([]uint8("405"))
	})

	r.GET("/test", dummy_handler, nil)
	r.GET("/test/<param>", param_handler, nil)
	r.GET("/any/*", any_handler, nil)
	r.GET("/ctx", ctx_handler, mux.NewContext(42))

	http.ListenAndServe("0.0.0.0:9999", r)
}

```